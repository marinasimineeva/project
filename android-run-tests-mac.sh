echo "Installing dependencies"
chmod 0755 requirements.txt

python3 -m pip install -r requirements.txt
pkill -9 -f appium
appium &


## Desired capabilities:
export PLATFORM="android"
export APP_ANDROID_PATH="./app-debug.apk"
export APPIUM_AUTOMATION="uiautomator2"


## Run the test:
echo "Running tests"

#rm -rf screenshots
python3 -m pytest tests

echo "Tests done"

echo "Clean - up"
pkill -9 -f appium
echo "Process finished"
