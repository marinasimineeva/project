from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from core.main.configs.Config import config_object


class Driver:
    def __init__(self, server_url, desired_caps):
        self.driver = webdriver.Remote(server_url, desired_caps)

    def init_webdriver(self):
        return self.driver

    def quit(self):
        self.driver.quit()

    def driver_find_element(self, element):
        return self.driver.find_element(By.ID, element)

    def wait_till_present_element(self, element):
        return WebDriverWait(self.driver, config_object.TIMEOUT).until(EC.presence_of_element_located((By.ID, element)))

    def click(self, element):
        element.click()
