from dataclasses import dataclass
@dataclass
class Device:
    platform_version: str
    device_name: str
    avd_name: str
