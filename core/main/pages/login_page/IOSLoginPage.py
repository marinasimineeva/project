from core.main.pages.login_page.LoginPage import LoginPageModel


class IOSLoginPage(LoginPageModel):
    def __init__(self):
        self.app_title_locator = "textView5"
        self.welcome_text_locator = "textView3"
        self.input_name_locator = "main_enter_name"
        self.start_button_locator = "main_start_button"

IOSLoginPage_object = IOSLoginPage()