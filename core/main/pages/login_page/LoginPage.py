from dataclasses import dataclass

@dataclass
class LoginPageModel:
    app_title_locator: str
    welcome_text_locator: str
    input_name_locator: str
    start_button_locator:str


class LoginPage:

    def __init__(self, driver, locators: LoginPageModel):
        self._app_title_locator = locators.app_title_locator
        self._welcome_text_locator = locators.welcome_text_locator
        self._input_name_locator = locators.input_name_locator
        self._start_button_locator = locators.start_button_locator
        self.driver = driver

    def app_title(self):
        return self.driver.wait_till_present_element(self._app_title_locator)

    def welcome_text(self):
        return self.driver.wait_till_present_element(self._welcome_text_locator)

    def input_name(self):
        return self.driver.wait_till_present_element(self._input_name_locator)

    def start_button(self):
        return self.driver.wait_till_present_element(self._start_button_locator)

    def checkloaded(self):
        assert (self.app_title().is_displayed())
        assert (self.welcome_text().is_displayed())
        assert (self.input_name().is_displayed())

    def clickStart(self):
        self.driver.click(self.start_button())
