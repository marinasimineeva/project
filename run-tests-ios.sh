#!/bin/bash

echo "Installing dependencies"
chmod 0755 requirements.txt

options=("--alluredir=./allure-results")
options+=("-m" "test")
options+=("-v")

DATE_BIN=$(command -v date)
DATE=`${DATE_BIN} +%Y-%m-%d-%T`


python3 -m pip install -r requirements.txt
pkill -9 -f appium
sleep 2
appium --use-plugins="images, execute-driver" &
sleep 10


## Desired capabilities:
export PLATFORM="ios"
export APP_IOS_PATH="./START.app"
export APPIUM_AUTOMATION="XCUITest"
export APPIUM_SERVER_ADDRESS=http://0.0.0.0:4723


## Run the test:
echo "Running tests"

echo "about to launch pytest ${options[@]}"
python3 -m pytest "${options[@]}"
echo "Tests done"

# generate allure reports
allure generate -c ./allure-results -o ./allure-report

# generate combined report in a single html (for history)
echo "attempting to create ./combined-reports/${DATE}"
allure-combine ./allure-report --dest ./combined-reports/${DATE} --auto-create-folders

echo "Killing appium and simulator"
killall Simulator
pkill -9 -f appium
sleep 5
#trap "exit" INT TERM
#trap "kill 0"
