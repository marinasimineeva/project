import pytest

from tests.CoreTestCase import CoreTestCase


class TestLogin(CoreTestCase):

    def test_login_page_is_displayed(self, init_pages):
        Page = init_pages
        Page.LoginPage.checkloaded()
        
    def test_click_on_login(self, init_pages):
        Page = init_pages
        Page.LoginPage.clickStart()

